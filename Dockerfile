FROM ubuntu:latest

ARG HOME

ARG DEBIAN_FRONTEND=noninteractive

ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN  apt-get update \
  && apt-get install -y wget \
  && apt-get install -yq curl \
  && apt-get install -yq apt-transport-https

RUN apt-get -yq update && apt-get -yq upgrade

RUN apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:ondrej/php
RUN apt-get update
RUN apt-get install -yq php7.3
  
# && rm -rf /var/lib/apt/lists/*

RUN useradd -ms /bin/bash  vault

WORKDIR $HOME

RUN chown vault:vault $HOME

USER vault

RUN wget http://aprelium.com/data/abwsx1.tgz

RUN tar xzfm ./abwsx1.tgz

WORKDIR $HOME/abyssws

